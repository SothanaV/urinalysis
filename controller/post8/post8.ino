#include <SPI.h>
#include <Mcp3208.h>
#include <Wire.h>
#include <Adafruit_MCP4725.h>

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

#define SPI_CS      15       // SPI slave select
#define ADC_VREF    3300     // 3.3V Vref
#define ADC_CLK     1600000  // SPI clock 1.6MHz

#include <Ethernet.h>
#include <ACROBOTIC_SSD1306.h>

MCP3208 adc(ADC_VREF, SPI_CS);
Adafruit_MCP4725 dac;

void SendData(uint16_t val_current,uint16_t val_volt);
const char* ssid     = "Chiravoot_laptop";                         //Set ssid
const char* password = "qwer1234";                    //Set Password
const char* Server   = "192.168.137.1";           //set Server Domain or Server ip
const char* port     = "5000";                       //set server port
const char* port2     = "8000";                       //set server port
ESP8266WiFiMulti WiFiMulti;

int swit = 16;
int val_sw = 0;
String Vstr = "" ; 
String Istr = "" ;

int abs(int x) 
{
  if(x<0){
    return -x;
  }
  return x;
}

int* read_sensor(uint16 *valueI, uint16 *valueV, int n) 
{
  int i;
  for(i=0; i<n; ++i) {
    //value[i] = 1; //read_analog_here
    valueI[i] = adc.toAnalog( adc.read(MCP3208::SINGLE_1) );
    valueV[i] = adc.toAnalog( adc.read(MCP3208::SINGLE_0) );
//    Serial.print("Read Value");
//    Serial.println(valueI[i]);
    //delay(1);
  }
}

float ransac(uint16  *value, int n, int t) 
{
  int i, j, maxi=0;
  int cnt[n], sum[n];
  for(i=0; i<n; ++i) {
    sum[i] = 0;
    cnt[i] = 0;
  }
  for(i=0; i<n; ++i) {
    for(j=0; j<n; ++j) {
      if(i!=j) {
        if(abs(value[i]-value[j]) < t){
          cnt[i] += 1;
          sum[i] += value[j];
        }
      }
    }
    if(cnt[i] > maxi) {
      maxi = i;
    }
  }
  Serial.print(sum[maxi]);
  Serial.print("   ");
  Serial.println(cnt[maxi]);
  return float(sum[maxi])/float(cnt[maxi]);
}
void readSensor(float *res)
{
  uint8 n = 50;
  uint8 ti = 50, tv = 50;
  uint16 valueI[n], valueV[n];
  read_sensor(valueI, valueV, n);
  res[0] = ransac(valueI, n, ti); 
  res[1] = ransac(valueV, n, tv);
}
void(* resetFunc) (void) = 0;
void setup() 
{
  Serial.begin(115200);
  pinMode(SPI_CS, OUTPUT);
  digitalWrite(SPI_CS, HIGH);
  SPISettings settings(ADC_CLK, MSBFIRST, SPI_MODE0);
  SPI.begin();
  SPI.beginTransaction(settings);
  dac.begin(0x64);
  Wire.begin();  
  oled.init();                      // Initialze SSD1306 OLED display
  oled.clearDisplay();              // Clear screen
  oled.setTextXY(2,0);              // Set cursor position, start of line 0
  oled.putString("  Go Tu Go Gout ");
  WiFiMulti.addAP(ssid, password);    //Set SSID and Password (SSID, Password)
  WiFi.begin(ssid, password);         //Set starting for Wifi
  pinMode(swit,INPUT);
  for(int t=5;t>0;t--)
  {
    Serial.println("[SETUP]:"+t);
    delay(1000);
  }
  Serial.println(WiFi.localIP());
  String str = "Hello";
  
}

void loop()
{
  Serial.println("Send_data");
    if((WiFiMulti.run() == WL_CONNECTED)) 
    {
        HTTPClient http;
        String str2 = "http://" +String(Server)+":"+String(port2)+"/data/wemos3/";
        Serial.println(str2);
        http.begin(str2);
        int httpCode2 = http.GET();
        Serial.print("[HTTP] GET... code:");
        Serial.println(httpCode2);
          if(httpCode2 > 0) 
          {
            if(httpCode2 == HTTP_CODE_OK) 
              {
                String payload2 = http.getString();
                Serial.println(payload2);
                if(payload2=="ready")
                {              
                  oled.setTextXY(2,0);              
                  oled.putString("  Go Tu Go Gout ");
                  oled.setTextXY(4,6);
                  oled.putString("READY!!");
                  delay(100);
                }
                val_sw = digitalRead(swit);
                if(payload2=="start"||val_sw==1)
                {
                  String str = "http://" +String(Server)+":"+String(port2)+"/data/wemos/";
                  Serial.println(str);
                  http.begin(str);
                  int httpCode = http.GET();
                  Serial.print("[HTTP] GET... code:");
                  Serial.println(httpCode);
                   if(httpCode > 0) 
                    {
                      if(httpCode) 
                      {
                          String payload = http.getString();
                          if(payload!="hello")
                          {
                            Serial.println(payload);
                          }
                          else
                          {
                            payload = "0000,0800,3300,0050,0100,0025,0005,0005,";
                          }
                          String vming = payload.substring(0,4);
                          String vmaxg = payload.substring(5,9);
                          String vccg  = payload.substring(10,14);
                          String pwg   = payload.substring(15,19);
                          String tg    = payload.substring(20,24);
                          String ag    = payload.substring(25,29);
                          String eg    = payload.substring(30,34);
                          String lg    = payload.substring(35,39);
                          float vmin  = vming.toFloat();
                          float vmax  = vmaxg.toFloat();
                          float vcc   = vccg.toFloat();
                          float pw    = pwg.toFloat();
                          float t     = tg.toFloat();
                          float a     = ag.toFloat();
                          float e     = eg.toFloat();
                          int L       = lg.toInt();
                          //Serial.println(vmin,vmax,vcc,pw,t,a,e);
                          Serial.print("Vmin:");Serial.println(vmin);
                          Serial.print("Vmax:");Serial.println(vmax);
                          Serial.print("Vcc:");Serial.println(vcc);
                          Serial.print("PulseWidth:");Serial.println(pw);
                          Serial.print("Period:");Serial.println(t);
                          Serial.print("Amplitude:");Serial.println(a);
                          Serial.print("StepE:");Serial.println(e);
                          Serial.print("Loop:");Serial.println(L);
          //                oled.clearDisplay();              
          //                oled.setTextXY(0,0);              
          //                oled.putString();
          //                oled.setTextXY(4,4);
          //                oled.putString("  READY!!  ");
                          oled.clearDisplay();
                          oled.putString("Vmin  :");oled.putString(vming);
                          oled.setTextXY(1,0);
                          oled.putString("Vmax  :");oled.putString(vmaxg);
                          oled.setTextXY(2,0);
                          oled.putString("Vcc   :");oled.putString(vccg);
                          oled.setTextXY(3,0);
                          oled.putString("PW    :");oled.putString(pwg);
                          oled.setTextXY(4,0);
                          oled.putString("T     :");oled.putString(tg);
                          oled.setTextXY(5,0);
                          oled.putString("A     :");oled.putString(ag);
                          oled.setTextXY(6,0);
                          oled.putString("StepE :");oled.putString(eg);
                          oled.setTextXY(7,0);
                          oled.putString("Loop  :");oled.putString(lg);
                          delay(6000);
                if(L>0)
                {
                  
                  oled.clearDisplay();
                  oled.setTextXY(2,0);              
                  oled.putString("  Go Tu Go Gout ");
                  oled.setTextXY(4,2);
                  oled.putString("Scaning...");
                  for(int k=0;k<L;k++)
                  {
                    Serial.print("Loop NO :");
                    Serial.println(k);
                    GenWave(vmin,vmax,vcc,pw,t,a,e);
                  }
                  oled.clearDisplay();
                oled.setTextXY(2,0);              
                oled.putString("  Go Tu Go Gout ");
                oled.setTextXY(4,2);
                oled.putString("!!Finish!!");
                delay(3000);
                
                   Serial.println("END LOOP");
                   HTTPClient http;
                   http.begin("http://" +String(Server)+":"+String(port2)+"/data/wemos2/");
                   http.addHeader("Content-Type", "application/x-www-form-urlencoded");
                   Serial.print(Istr);
                   http.POST("V="+Vstr+"&"+"I="+Istr);
//                   http.writeToStream(&Serial);
                  Vstr = "";
                  Istr = "";
                  for(int i =0;i<100;i++)
                  {
                      HTTPClient http;
                      String str_u = "http://" +String(Server)+":"+String(port2)+"/geturine/";
                      Serial.println(str_u);
                      http.begin(str_u);
                      int httpCode_u = http.GET();
                      String payload_u = http.getString();
                      oled.clearDisplay();
                      oled.setTextXY(2,0);              
                      oled.putString("  Go Tu Go Gout ");
                      oled.setTextXY(4,0);
                      oled.putString("Urine :");
                      oled.putString(payload_u);
                      oled.putString("mM");
                      oled.setTextXY(5,7);
                      float u2 = payload_u.toFloat();
                      float u = float(u2)*10.0*16.9;
                      oled.putString(String(u));
                      oled.putString("mg/dL");
                      Serial.println(payload_u);
                      delay(1000);
                      if(digitalRead(swit) == 1)
                      {
                        break;
                      }
                  }
                }
                }
                delay(5000);
                oled.clearDisplay();
              }
              //else{digitalWrite(led,LOW);}
          }
          //else{digitalWrite(led,LOW);}
       }
   
       //else{digitalWrite(led,LOW);}
     }
     //else{digitalWrite(led,LOW);}
   }
   //else{digitalWrite(led,LOW);}
}

void GenWave(float vmin,float vmax,float vcc,float pw, float t,float a,float e)
{
      float v = vmax;
//      uint16_t valI1,valI2, valV, dI;
      float dI, valV;
      float res1[2], res2[2];
      int DI;
      int VI;
      while(1)
      {
        v = v-(2*a)-e; if(v<vmin) break;
        int x = (int)((v/vcc)*4095.0f);
        dac.setVoltage(x, false);
        readSensor(res1);
        delay(pw);
        v=v+(2*a); if(v<vmin) break;
        x = (int)((v/vcc)*4095.0f);
        dac.setVoltage(x, false);
        delay(t-pw);
        readSensor(res2);
        dI = res1[0] - res2[0];
        DI = (int)(dI*100);
        valV = (res1[1]+res2[1])/2;
        VI =(int)(valV*100);
        Serial.println();
        Serial.print(dI);
        Serial.print("    ");
        Serial.println(valV);
        Istr = Istr + DI + ",";
        Vstr = Vstr + VI + ",";
        
      } 
}
